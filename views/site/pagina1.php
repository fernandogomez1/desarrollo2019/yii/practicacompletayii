<?php

/* @var $this yii\web\View */

use app\models\Noticias;
use yii\db\Query;
use yii\db\Command;
use yii\base\Component;
use yii\base\BaseObject;
use yii\db\Connection;

?>
<div class="row">
    <div class="col-lg-4 fondoOscuro">
        <?= \yii\helpers\Html::img("@web/img/foto2.jpg") ?>
    </div>
    <div class="col-lg-8 fondoOscuro">
         
                <?php
                //obtener array con todos los resultados.
                $noticias = Noticias::find()->all();
                //genera numero aleatorio para sacar el id de la noticia
                $r=random_int(1, count($noticias));
                //Haciendo consulta de noticia aleatoria
                $noticias = Noticias::find()->where(['id'=>$r])->one();
                ?>
                <h2><?php echo $noticias['titulo'] ?></h2>

                <p><?php echo $noticias['texto'] ?></p>
               
    </div>
</div>
