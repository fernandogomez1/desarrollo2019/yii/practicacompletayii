<?php

/* @var $this yii\web\View */

use app\models\Noticias;

use yii\db\Query;

//para usar command
use yii\db\Command;
use yii\base\Component;
use yii\base\BaseObject;

use yii\db\Connection;


$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="row">
        <div class="col-lg-12 fondoOscuro">
            <span>Home</span>
            <span>Pagina1</span>
        </div>
         <div class="col-lg-12 separador">
            
        </div>
    </div>
    <div class="jumbotron fondoOscuro">
        <h1>Ejemplo 1 de aplicacion</h1>

        <p class="lead">Podemos ver un ejemplo del funcionamiento de este framework.</p>

        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-5 fondoOscuro">
                     <?php 
                     //consulta con clase query para columna 1
                    $query = new Query;
                    // compose the query
                    $query->select('titulo, texto')
                        ->from('noticias')
                        ->limit(10);
                    // build and execute the query
                    $rows = $query->all();
                    // alternatively, you can create DB command and execute it
                    $command = $query->createCommand();
                    // $command->sql returns the actual SQL
                    $rows = $command->queryAll();
                    //var_dump($rows);
                    //var_dump($rows[0]);
                    $f1=$rows[0];
                    $f11=$rows[0];
                    
                    //echo $f1['titulo'];
                    //echo $f1['texto'];
                    ?>
                <h2><?php echo $f1['titulo']; ?></h2>

                <p><?php echo $f1['texto']; ?></p>
               
              
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-5 fondoOscuro">
                <?php
                //Consulta usando la clase activeRecord
                $noticias = Noticias::find()->where(['id'=>2])->one();
                      

                // this will get related records from orders table when relation is defined
                
                //var_dump($noticias);
                //var_dump($noticias['titulo']);
                //consulta con command para columna 2
                
                /*$command = $connection->createCommand('SELECT * FROM post');
                $posts = $command->queryAll();
                $command = $connection->createCommand('UPDATE post SET status=1');
                $command->execute();
                $consulta = $connection->createCommand('SELECT titulo, texto FROM noticias')->queryAll();*/

                ?>
                <h2><?php echo $noticias['titulo'] ?></h2>

                <p><?php echo $noticias['texto'] ?></p>
               
            </div>
          
        </div>

    </div>
</div>
